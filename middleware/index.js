const Validator = require("validatorjs");
const strCodes = require("../alpha2");
const middlewareObj = {};

middlewareObj.postAddress = function (req, res, next) {
  const data = ({
    country,
    city,
    street,
    postalcode,
    number,
    numberAddition,
  } = req.body);

  // Setup validatorJS rules
  // https://www.npmjs.com/package/validatorjs
  const rules = {
    country: ["required", "string", `in:${strCodes}`],
    city: ["required", "string"],
    street: ["required", "string"],
    postalcode: ["required", "regex:/^\\d{5}$/"],
    number: ["required", "integer", "min: 0"],
    numberAddition: ["string"],
  };
  // Run Validation
  const validation = new Validator(data, rules);
  if (validation.passes()) return next();
  else {
    res
      .status(422)
      .json({ error: true, message: "Input data in incorrect format." });
  }
};

middlewareObj.editAddress = function (req, res, next) {
  const { status, name, email } = req.body;
  const rules = {
    status: ["required", "string", "in:not at home,not interested,interested"],
    name: "string",
    email: ["string", "email"],
  };
  // Run Validation
  const validation = new Validator(data, rules);
  if (validation.passes()) return next();
  else {
    res
      .status(422)
      .json({ error: true, message: "Input data in incorrect format." });
  }
};

module.exports = middlewareObj;
