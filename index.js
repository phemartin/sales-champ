const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const seedDB = require("./seeds");
const app = express();

// Body parser middleware
app.use(bodyParser.json());
app.use(express.json());

// Connect to MongoDB
const mongoURI = "mongodb://127.0.0.1";
mongoose
  .connect(`${mongoURI}/salesChamp?retryWrites=true`, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB Connected..."))
  .catch((error) => console.log(error));

// Seed Database with starting information
seedDB();

// Routes
const addressRoutes = require("./routes/address");
app.use("/address", addressRoutes);

// Ports
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server started at port ${port}! `));
