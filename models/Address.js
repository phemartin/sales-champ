const mongoose = require("mongoose");

// Create Schema
const AddressSchema = new mongoose.Schema({
  country: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  street: {
    type: String,
    required: true,
  },
  postalcode: {
    type: String,
    required: true,
    minlength: 5,
  },
  number: {
    type: Number,
    required: true,
  },
  numberAddition: {
    type: String,
    default: "",
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  status: {
    type: String,
    default: null,
  },
  name: {
    type: String,
    default: null,
  },
  email: {
    type: String,
    default: null,
  },
});

module.exports = mongoose.model("Address", AddressSchema);

// " id ": " 581b5b28f3bc7b88210c4fe2 ",
// " country ": " CZ ",
// " city ": " Brno ",
// " street ": " Husova ",
// " postalcode ": " 60200 ",
// " number ": 6,
// " numberAddition ": "",
// " createdAt ": " 2016 - 11 - 03T15 : 22 : 31Z ",
// " updatedAt ": " 2016 - 11 - 03T15 : 22 : 31Z ",
// " status ": null,
// " name ": null,
// " email ": null
