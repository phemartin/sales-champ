const express = require("express");
const middleware = require("../middleware");
const Address = require("../models/Address");
const router = express.Router();

// @route GET /address
// @desc Fetch all addresses
// @access Public
router.get("/", (_req, res) => {
  Address.find()
    .sort({ createdAt: -1 })
    .then((addresses) => res.status(200).json(addresses))
    .catch((err) => res.status(400).json({ message: err.message }));
});

// @route POST /address
// @desc Create an address
// @access Public
router.post("/", middleware.postAddress, (req, res) => {
  const newAddress = new Address(
    ({ country, city, street, postalcode, number, numberAddition } = req.body)
  );
  // Post to db
  newAddress
    .save()
    .then((address) => res.status(201).json(address))
    .catch((err) => res.status(400).json({ message: err.message }));
});

// @route GET /address/{id}
// @desc Fetch an address with Id
// @access Public
router.get("/:id", (req, res) => {
  Address.findById(req.params.id)
    .then((address) => res.status(200).json(address))
    .catch((err) =>
      res.status(404).json({ error: true, message: "Address does not exist." })
    );
});

// @route PATCH /address/{id}
// @desc Edit an address
// @access Public
router.patch("/:id", async (req, res) => {
  const { status, name, email } = req.body;
  let address;
  try {
    address = await Address.findById(req.params.id);
    // Check if changes are allowed
    if (["not interested", "interested"].includes(address.status)) {
      res
        .status(403)
        .json({ error: true, message: "No further changes allowed." });
    }
    // Update values
    address.status = status;
    address.updatedAt = new Date();
    if (name) address.name = name;
    if (email) address.email = email;
    // Update db
    address = await address.save();
    res.status(200).json(address);
  } catch (err) {
    if (!address) {
      res.status(404).json({ error: true, message: "Address does not exist." });
    } else {
      res.status(500).json({
        error: true,
        message: "Address couldn't be updated. Please try again.",
      });
    }
  }
});

// @route DELETE /address/{id}
// @desc Delete an address
// @access Public
router.delete("/:id", async (req, res) => {
  let address;
  try {
    address = await Address.findById(req.params.id);
    address.delete();
    res.status(204).json({});
  } catch (err) {
    if (!address) {
      res.status(404).json({ error: true, message: "Address does not exist." });
    } else {
      res.status(409).json({
        error: true,
        message: "Address couldn't be deleted. Please try again.",
      });
    }
  }
});

module.exports = router;
