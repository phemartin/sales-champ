const mongoose = require("mongoose");
const Address = require("./models/Address");

const data = {
  country: "CZ",
  city: "Brno",
  street: "Husova",
  postalcode: "60200",
  number: 6,
  numberAddition: "",
};

// Delete all old records and seed db with starting data
async function seedDB() {
  // Drop entire collection
  Address.collection.drop();
  // Add data
  const seedAddress = new Address(data);
  await seedAddress.save();
  return;
}

module.exports = seedDB;
